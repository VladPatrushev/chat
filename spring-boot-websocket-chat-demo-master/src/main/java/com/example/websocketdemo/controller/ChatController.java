package com.example.websocketdemo.controller;

import com.example.websocketdemo.model.ChatMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@Controller
public class ChatController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    // Когда тыкаем на послать сообщение определ юзеру мы добавляем его в сессию( по его id)
    // добав его в сессию
    public ChatMessage addUser(@Payload ChatMessage chatMessage,
                               SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        return chatMessage;
    }

    @MessageMapping("/chat.message")
    //@SendToUser(value= "")
    public ChatMessage filterMessage(@Payload ChatMessage message, Principal principal) {

        message.setSender(principal.getName());
        return message;
    }

    //Test
    @MessageMapping("/chat.private.{username}")
    @SendTo("/topic/public")
    public ChatMessage filterPrivateMessage(@Payload ChatMessage message, @DestinationVariable("username") String username, Principal principal){


          message.setSender(principal.getName());
         // simpMessagingTemplate.convertAndSend("/user/"+ username + "/exchange/direct/chat.message", message);
       // simpMessagingTemplate.convertAndSend("/chat.sendMessage", message);
        return message;

    }

    //Если захотим модерировать сообщения/
    // For instance : если в чате дети, сообщения с матами не дойдут
    //TODO
    private void moderationMethod(ChatMessage message){

    }


}
